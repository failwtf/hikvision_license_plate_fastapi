FROM python:3.10-slim

MAINTAINER DENYS

#VOLUME license_plate_db:/home/hikvision_license_plate_fastapi/db

EXPOSE 10001 80

ENV TZ=Europe/Kiev

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install git
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git

WORKDIR /home

RUN git clone https://gitlab.com/failwtf/hikvision_license_plate_fastapi.git

WORKDIR /home/hikvision_license_plate_fastapi

RUN pip install --no-cache-dir --upgrade -r ./requirements.txt

CMD ["uvicorn", "main:app", "--reload", "--host", "0.0.0.0", "--port", "80"]
