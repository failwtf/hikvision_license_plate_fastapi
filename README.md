## Sample Code of Receiving Alarm/Event in Listening Mode:
https://tpp.hikvision.com/Wiki/ISAPI/Access%20Control%20on%20Person/GUID-0E00BD39-BDB4-4079-A8EE-307A95871C1A.html


### Work with Binary Picture Data and without Binary Picture Data.
#### Save images, view images!

### 1. Method save data in db: POST http://localhost:10001/send_event_notification

### 2. Method get data in db: GET http://localhost:10001/get_event_notifications?start_datetime=2022-10-01T07:42:14&end_datetime=2022-10-30T23:59:59
### If used method without parameters, all data will be returned!
### Param: view_notifications: bool, default False. Returned response json or Jinja2Templates VIEW!
 
### 3. Method delete data in db and images in the disk: GET http://localhost:10001/delete_event_notifications?start_datetime=2023-03-16T00:00:00&end_datetime=2023-03-16T23:59:59
### If used method without parameters, all data and images in the disk will be deleted!

### 4. Method make vacuum for db: GET http://localhost:10001/make_vacuum

### 5. Method get view images: GET http://localhost:10001/get_images/20a31ee4-1dd2-11b2-bc0b-e34e266fc623

### 6. Method delete images in the disk from UUID: GET http://localhost:10001/delete_images?uuid=20a31ee4-1dd2-11b2-bc0b-e34e266fc6231

### 7. Method delete images in the disk from period: GET http://localhost:10001/delete_images?start_datetime=2023-03-16T00:00:00&end_datetime=2023-03-16T23:59:59

### 8. Method index for view event_notifications from period: GET http://localhost:10001/index


## Configureted hikvision camera.
### Configuration -> Network -> Data Connection -> ISAPIListen: 
### 1. ANPR IP/Domain: ip adress "hikvision_license_plate_fastapi"
### 2. ANPR Port: port "hikvision_license_plate_fastapi"
### 3. Host URL: /send_event_notification
### 3. Upload Binary Image: false

## How to run with Docker:

#### 1. git clone https://gitlab.com/failwtf/hikvision_license_plate_fastapi.git
#### 2. cd .\hikvision_license_plate_fastapi\
#### 3. docker-compose up


## How to run with PowerShell in Win:

#### 1. git clone https://gitlab.com/failwtf/hikvision_license_plate_fastapi.git
#### 2. cd .\hikvision_license_plate_fastapi\
#### 3. python3.10 -m venv .env
#### 4. .\\.env\Scripts\Activate.ps1
#### 5. pip install -r .\requirements.txt
#### 6. uvicorn main:app --reload --host 0.0.0.0 --port 8080
