from datetime import datetime

from typing import Optional

from sqlmodel import Field, SQLModel


class EventNotification(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    date_time_event_alert: str
    date_time_ANPR: str
    uuid: str
    device_uuid: str
    device_id: str
    license_plate: str


def GetEventNotification(xml_data: dict):

    EventNotificationAlert = xml_data.get('EventNotificationAlert', None)
    if EventNotificationAlert:

        ANPR = EventNotificationAlert.get('ANPR', None)
        if ANPR:

            eventNotification = EventNotification()

            dateTimeANPR = EventNotificationAlert.get('dateTime', None)

            eventNotification.date_time_event_alert = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            eventNotification.date_time_ANPR        = datetime.fromisoformat(dateTimeANPR).strftime("%Y-%m-%d %H:%M:%S")

            eventNotification.uuid          = EventNotificationAlert.get('UUID',        None)
            eventNotification.device_uuid   = EventNotificationAlert.get('deviceUUID',  None)
            eventNotification.device_id     = EventNotificationAlert.get('deviceID',    None)

            eventNotification.license_plate = ANPR.get('licensePlate', None)

            return eventNotification

    return None