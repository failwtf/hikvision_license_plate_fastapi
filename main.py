# debug test -->
import uvicorn
# <-- debug test

from datetime import datetime

from models import EventNotification, GetEventNotification
from database import create_db_and_tables, get_session
from core_library import get_anpr_xml, save_images, get_images_data, delete_images_data

from fastapi import FastAPI, Request, Depends, HTTPException, Form
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from sqlmodel import Session, select



app         = FastAPI()
templates   = Jinja2Templates(directory="templates")



@app.on_event("startup")
def on_startup():
    create_db_and_tables()



@app.get("/index", response_class=HTMLResponse)
def get_view_notifications():

    templat = templates.get_template("index.html")
    return templat.render()


@app.post("/send_event_notification")
def send_event_notification(*,  session: Session = Depends(get_session),
                                        data_list = Depends(get_anpr_xml)):

    xml_data        = data_list["xml_data"]
    image_data_list = data_list["image_data_list"]

    if xml_data:

        eventNotification = GetEventNotification(xml_data)
        if eventNotification:

            session.add(eventNotification)
            session.commit()
            session.refresh(eventNotification)

            if image_data_list:
                save_images(eventNotification.uuid, image_data_list)

            return eventNotification
        
    else:
        raise HTTPException(status_code=404, detail='Something went wrong') 


@app.get("/get_event_notifications")
def get_event_notifications(request: Request,  
                            session: Session = Depends(get_session),
                            start_datetime: datetime | None = None,
                            end_datetime:   datetime | None = None,
                            view_notifications: bool | None = False):

    if not start_datetime or not end_datetime:
        eventNotifications = session.exec(select(EventNotification)).all()
    else:

        start_datetime_str  = start_datetime.strftime("%Y-%m-%d %H:%M:%S")
        end_datetime_str    = end_datetime.strftime("%Y-%m-%d %H:%M:%S")

        statement = select(EventNotification).where(EventNotification.date_time_ANPR >= start_datetime_str,
                                                    EventNotification.date_time_ANPR <= end_datetime_str)

        eventNotifications = session.exec(statement).all()

    if view_notifications:
        return templates.TemplateResponse("view_event_notifications.html", {"request": request, "event_notifications": eventNotifications})
    else:
        return eventNotifications


@app.get("/delete_event_notifications")
def delete_event_notifications(*, session: Session = Depends(get_session),
                            start_datetime: datetime | None = None,
                            end_datetime: datetime | None = None):

    if not start_datetime or not end_datetime:
        
        eventNotifications = session.query(EventNotification).all()
        # session.query(EventNotification).delete()
        for notification in eventNotifications:
            session.delete(notification)
            delete_images_data(notification.uuid)

    else:
        
        start_datetime_str  = start_datetime.strftime("%Y-%m-%d %H:%M:%S")
        end_datetime_str    = end_datetime.strftime("%Y-%m-%d %H:%M:%S")
        
        statement = select(EventNotification).where(EventNotification.date_time_ANPR >= start_datetime_str,
                                                    EventNotification.date_time_ANPR <= end_datetime_str)
        
        eventNotifications = session.exec(statement).all()
        for notification in eventNotifications:
            session.delete(notification)
            delete_images_data(notification.uuid)

    if len(eventNotifications) != 0:
        try:
            session.commit()
            session.execute("VACUUM")
            return eventNotifications
        except:
            session.rollback()
            raise HTTPException(status_code=500, detail="Failed to delete event notifications.")
    else:    
        raise HTTPException(status_code=404, detail="No search event notifications to delete.")


@app.get("/make_vacuum")
async def vacuum(*, session: Session = Depends(get_session)):
   
    session.execute("VACUUM")
    return {"message": "VACUUM executed successfully."}


@app.get("/get_images/{uuid}", response_class=HTMLResponse)
def get_images(request: Request, uuid: str):
   
    image_data = get_images_data(uuid)

    if image_data and not type(image_data) == str:
        return templates.TemplateResponse("images.html", {"request": request, "images": image_data})
    else:
        raise HTTPException(status_code=404, detail=image_data)    


@app.get("/delete_images")
def delete_images(*,    session:        Session = Depends(get_session),
                        uuid:           str | None = None,
                        start_datetime: datetime | None = None,
                        end_datetime:   datetime | None = None):
    
    if uuid != None:

        if delete_images_data(uuid):
            return {"message": f"Images directory for {uuid} deleted successfully"}
        else:
            raise HTTPException(status_code=404, detail=f"Images directory not found for {uuid}")

    elif start_datetime != None and end_datetime != None:
        
        data_request = {}

        start_datetime_str  = start_datetime.strftime("%Y-%m-%d %H:%M:%S")
        end_datetime_str    = end_datetime.strftime("%Y-%m-%d %H:%M:%S")
        
        statement = select(EventNotification).where(EventNotification.date_time_ANPR >= start_datetime_str,
                                                    EventNotification.date_time_ANPR <= end_datetime_str)
        
        eventNotifications = session.exec(statement).all()
        for notification in eventNotifications:
            if delete_images_data(notification.uuid):
                data_request[notification.id] = {"status_code=200": f"Images directory for {notification.uuid} deleted successfully"}
            else:
                data_request[notification.id] = {"status_code=404": f"Images directory not found for {notification.uuid}"}

        return data_request
    
    raise HTTPException(status_code=404, detail=f"No search parameters to delete images")



# debug test -->
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
# <-- debug test