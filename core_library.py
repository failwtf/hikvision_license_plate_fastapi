import json
import base64
import shutil
import xmltodict

from pathlib import Path

from datetime import datetime

from fastapi import Request


async def get_anpr_xml(request: Request):

    # await save_request_data(request)

    data_list = {}

    data_list["xml_data"] = None
    data_list["image_data_list"] = None

    content_type = request.headers['Content-Type']

    if 'multipart/form-data' in content_type:

        form = await request.form()
        anpr_xml = form.get("anpr.xml", None)

        if anpr_xml and not type(anpr_xml) == str:
            # filename = anpr_xml.filename
            contents = await anpr_xml.read()
            xml_data = xmltodict.parse(contents)

            # return xml_data
            data_list["xml_data"] = xml_data

        image_data_list = {}

        licensePlatePicture = form.get("licensePlatePicture.jpg", None)

        if licensePlatePicture and not type(licensePlatePicture) == str:
            licensePlatePicture_contents = await licensePlatePicture.read()

            image_data_list["licensePlatePicture.jpg"] = licensePlatePicture_contents

        detectionPicture = form.get("detectionPicture.jpg", None)

        if detectionPicture and not type(detectionPicture) == str:
            detectionPicture_contents = await detectionPicture.read()

            image_data_list["detectionPicture.jpg"] = detectionPicture_contents

        data_list["image_data_list"] = image_data_list

    return data_list


async def save_request_data(request) -> None:

    # Get the current date and time
    now = datetime.now()
    timestamp = now.strftime("%Y_%m_%d__%H_%M_%S")

    # Create the directory for the request data if it does not exist
    path_request_dir = Path(f"./requests/{timestamp}")
    path_request_dir.mkdir(parents=True, exist_ok=True)

    # Save the request data to a file with the current timestamp as the name
    path_request_data = path_request_dir / f"request_data.txt"
    path_request_form_data = path_request_dir / f"request_form_data.txt"

    data = {
        "method": request.method,
        "url": str(request.url),
        "headers": dict(request.headers),
        "query_params": dict(request.query_params),
        "cookies": dict(request.cookies),
        # "body": await request.body(),
        "client": request.client,
    }

    with open(path_request_data, 'w') as f:
        json.dump(data, f)

    form_data = {}
    form = await request.form()

    for field in form.keys():
        form_data[field] = field

    with open(path_request_form_data, 'w') as f:
        json.dump(form_data, f)


# {"anpr.xml": "anpr.xml", "licensePlatePicture.jpg": "licensePlatePicture.jpg", "detectionPicture.jpg": "detectionPicture.jpg"}


# def save_images(uuid: str, image_data_list: List[bytes]) -> None:
def save_images(uuid: str, image_data_list: dict) -> None:

    # Create the directory for the images if it does not exist
    path_images_dir = Path(f"./db/images/{uuid}")
    path_images_dir.mkdir(parents=True, exist_ok=True)

    # Save the images to the directory
    for key in image_data_list.keys():

        path_image = path_images_dir / f"{key}"
        with open(path_image, "wb") as f:
            f.write(image_data_list[key])


def get_images_data(uuid: str):

    path = Path(f"./db/images/{uuid}")
    if not path.is_dir():
        return f"Images directory not found for {uuid}."

    images = list(path.glob("*.jpg"))
    if not images:
        return f"Images not found for {uuid}."

    # image_data = []
    image_data = {}
    for image in images:
        with open(image, "rb") as file:
            image_bytes = file.read()
            image_base64 = base64.b64encode(image_bytes).decode("utf-8")
            # image_data.append(image_base64)
            image_data[Path(file.name).stem] = image_base64
            # image_data.append({Path(file.name).stem: image_base64})

    return image_data


def delete_images_data(uuid: str):
    image_dir = Path(f"./db/images/{uuid}")
    if image_dir.exists():
        shutil.rmtree(image_dir)
        return True
    else:
        return False
